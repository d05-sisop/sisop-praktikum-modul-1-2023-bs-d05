#!/bin/bash

#Membuat variabel date yang akan menyimpan data waktu
date=$(date '+%Y/%m/%d %H:%M:%S')

#Tampilan awal yang bisa dipilih user untuk login atau regist
echo -e "\nWelcome!!!"
echo -e "\nChoose Your Option"
echo -e "\nLogin (1) or Register (2)"
read command 

#Jika command 1 yang artinya login, akan diarahkan untuk login
if [ $command == 1 ]; then
	echo -e "\nLogin Your Account"
	echo -e "\nEnter your username: "
	read username
	echo -e "\nEnter your password: "
	read -s password #Untuk menutupi preview password yang akan diinput

	#Memeriksa apakah user sudah pernah regist atau belum dengan mengecek pada user.txt
	if grep -q -w "${username}" user.txt ; then # -q untuk tidak menampilkan output pada layar, -w untuk mencocokkan kata yang tepat atau whole word
		#Memeriksa validitas password dari user tersebut
		if grep -q -w "${username} ${password}" user.txt; then
			echo -e "\n${date} LOGIN SUCCESS" #Menampilkan pesan berhasil jika sudah benar memasukkan password
    			echo -e "\n${date} LOGIN: INFO User ${username} logged in" >> log.txt #Waktu dan message akan diprint dalam log.txt
		else 
			echo -e "\n${date} WRONG PASSWORD" #Jika password salah akan menampilkan pesan salah
			echo -e "\n${date} LOGIN: ERROR Failed login attempt on user ${username}" >> log.txt #Message juga akan diprint dalam log.txt
		fi

	#Jika user belum pernah regist, akan menampilkan pesan demikian dan diarahkan ke louis.sh untuk regist terlebih dahulu
	else
		echo -e "\nERROR: User does not exist"
		echo "You have to regist first"
		bash louis.sh
	fi

#Command 2 bisa dipilih user untuk langsung registrasi
elif [ $command == 2 ]; then
	bash louis.sh

#Jika command tidak sesuai dengan pilihan command, akan menampilkan pesan demikian 
else
	echo -e "\nOption not available"

fi
