#!/bin/bash

#a
#Menampilkan 5 universitas dengan ranking tertinggi di Jepang
awk -F',' '/Japan/ {print $2}'  University_Rankings.csv | head -5
#Menggunakan sintax awk 

#b
#Menampilkan universitas dengan fsr terendah dari 5 universitas yang telah di filter pada poin a.
echo -e '\nnumber2'
LC_ALL=C sort -t',' -k9n University_Rankings.csv | awk -F',' '/Japan/ {print $2}' | head -5

#c
#Menampilkan 10 universitas dengan Emplyment Outcome Rank paling tinggi
echo -e '\nnumber3'
sort -t',' -k20 -n University_Rankings.csv | awk -F',' '/Japan/ {print $2}' | head -10

#d
#Mencariuniversitas dengan kata kunci "keren"
echo -e '\nnumber4'
awk -F',' '/Keren/ {print $2}' University_Rankings.csv
