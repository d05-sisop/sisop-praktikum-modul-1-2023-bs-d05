# sisop-praktikum-modul-1-2023-BS-D05

## --- soal1:
		---university.sh

a.  Menampilkan 5 Universitas dengan ranking tertinggi di Jepang

	`awk -F ',' '/Japan/ {print$2} University_Rankings.csv`| head -5'

- Pembuatan script untuk menampilkan 5 universitas dengan ranking tertinggi di jepang diawali dengan syntax _awk_ untuk memindahkan text diikuti oleh _-F ','_ sebagai separatornya dengan argumen Japan yang diambil dari kolom kedua _{print $2}_ sehingga program hanya akan mengambil data dari kolom kedua yaitu universitas dan dipilih hanya universitas yang berada di Jepang saja.

- _University_Rankings.csv_ menjadi file yang akan diambil datanya. Kemudian menggunakan pipe yang berarti, semua data universitas yang berada di jepang yang merupakan output dari sebelum pipe digunakan sebagai input untuk syntax setelah pipe yaitu, _head -5_ dengan mengambil 5 baris teratas pada data universitas di Jepang.


![IMAGE_DESCRIPTION](Screenshoot ouput no 1/1a.png)

b. Menampilkan FSR paling rendah diantara 5 universitas yang telah difilter pada poin a

	`echo -e '\nnomor2'
	LC_ALL=C sort -t',' -k9n University_Rankings.csv | -F',' '/Japan/ {print $2}' | head -5`

- _echo_ akan mencetak text selanjutnya, dimana _-e_ berarti "enable the interpretation" atas text yang berada diantara single quote yang seharusnya menjadi text tidak mempunyai makna khusus.

- _LC_ALL=C_ yang akan mengesampingkan semua variabel lain dalam prioritas dan yang pertama akan menjadi variabel yang pertama kali akan di periksa oleh sitem atau menjadi prioritas utama yaitu sorting.

- _sort -t',' -k9n University_Rankings.csv_ yang akan mensorting kolom ke 9 dengan jenis sortingan yaitu numeric, -t',' sebagai separator. Data diambil dari file _University_Rankings.csv_.

- Menggunakan pipe dimana hasil sortingan kolom ke 9 dari data di file csv dijadikan input pada syntax setelahnya kemudian dipilih universitas yang terletak di Jepang dan terakhir, outputnya dijadikan input di syntax terakhir lalu dicari baris terakhir yaitu dari 5 universitas dengan rangking tertinggi yang memiliki nilai GER terendah.

![IMAGE_DESCRIPTION](Screenshoot ouput no 1/1b.png)

c. Menampilkan 10 universitas di Jepang dengan GER rank paling tinggi

	`echo -e '\nnomor'
	sort -t',' -k20 -n University-Rankings.csv | awk -F',' '/Japan/ {print $2}' | head -10`

- Penjelasan kurang lebih sama dengan sebelumnya.

![IMAGE_DESCRIPTION](Screenshoot ouput no 1/1c.png)

d. Menampilkan universitas dengan kata kunci "Keren"

	`echo -e '\nnomor4'
	awk -F',' /Keren/ {print $2}' University_Rankings.csv`

- Penjelasan kurang lebih sama dengan sebelumnya, tetapi disini argumen diganti dengan "keren" karena yang akan dicari adalah universitas dengan kata kunci keren.

![IMAGE_DESCRIPTION](Screenshoot ouput no 1/1d.png)


## --- soal2:
		---kobeni_liburan.sh

### Penjelasan
- Membuat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst). 
- Apabila pukul 00:00 cukup download 1 gambar saja. 
- Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. 
- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst). 
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst).

### Penyelesaian
-	```
	time_now=$(date +"%H")
	```

	- Mengambil input jam saat ini.

-	```
	if [ $time_now == 00 ]
	then
		x=1
	else
		x=$time_now
	fi
	echo "x adalah $x"
	```

	- Menggunakan if else jika jam saat ini 00:00 maka x = 1, selain itu maka x = jam saat ini.

-	```
	currfolder=$(ls -d kumpulan_* | wc -l)
	nextfolder=$((currfolder + 1))
	```

	- Menghitung jumlah directory untuk menentukan nama directory kumpulan. 
	- Menggunakan `ls` dan `wc` untuk mengecek terlebih dahulu jumlah directory kumpulan yang telah terbuat di repository lalu memasukkan jumlah kedalam variable 		`currfolder`. 
	- Kemudian `currfolder` ditambah dengan satu dan disimpan dalam `nextfolder` untuk menyimpan urutan folder selanjutnya yang harus dibuat saat ini.

-	```
	for ((num=1; num<=x; num=num+1))
	do
		curl https://www.panorama-destination.com/wp-content/uploads/2019/04/Mountain-Bromo-2.jpg --create-dirs -o $PWD/kumpulan_$nextfolder/perjalanan_$num.jpg
	done
	```
	
	- Melakukan perulangan for sebanyak X kali. 
	- Tiap perulangan akan mendownload gambar dari url yang telah ditentukan menggunakan fungsi `curl`. 
	- `--create-dirs -o` digunakan untuk membuat directory yang akan menjadi output gambar yang didownload. 
	- Folder menggunakan input `nextfolder` yang telah dibuat. Nama file menggunakan input `num` sesuai urutan perulangan.

-	```
	0 */10 * * * $PWD/kobeni_liburan.sh
	```
	> Cron Jobs Settings

	- Menggunakan cron jobs untuk menjalankan script download gambar setiap 10 jam sekali.


-	```
	zip.sh
	currzip=$(ls -d devil_* | wc -l)
	nextzip=$((currzip + 1))
	zip -r devil_$nextzip kumpulan_*
	rm -r kumpulan*
	```

	- Untuk melakukan zip menggunakan script `zip.sh`.
	- Menggunakan `ls` dan `wc` untuk mengecek terlebih dahulu jumlah folder devil yang telah terbuat di repository lalu memasukkan jumlah kedalam variable 	`currzip`.
	- Kemudian `currzip` ditambah dengan satu dan disimpan dalam `nextzip` untuk menyimpan urutan folder selanjutnya yang harus dibuat saat ini.
	- Fungsi `zip -r` digunakan untuk melakukan zip seluruh file yang terdapat pada semua folder dengan awalan `kumpulan_`
	- Menghapus semua folder kumpulan setelah melakukan zip dengan menggunakan `rm -r`

-	```
	0 0 * * * $PWD/zip.sh
	```
	> Cron Jobs Settings

	- Menggunakan crob jobs untuk menjalankan script `zip.sh` sehari sekali.

## --- soal3:
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file user.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh

Pada file retep.sh terdapat command yang bisa dipilih user untuk login atau register, command 1 untuk login dan command 2 untuk register. -e digunakan agar bisa mencetak endl.
```
echo -e "\nWelcome!!!"
echo -e "\nChoose Your Option"
echo -e "\nLogin (1) or Register (2)"
read command 
```

Jika user memilih login, user harus memasukkan username dan password. Sistem akan mengecek terlebih dahulu apakah username dan sudah pernah diregistrasi atau belum dengan memeriksanya pada file user.txt
```
if grep -q -w "${username}" user.txt ; then
```

Perintah grep digunakan untuk mencari string dalam file .txt. -q digunakan untuk tidak menampilkan output dan -w akan mencari string yang tepat sama (whole word) pada file tersebut.

Jika username sudah ada pada user.txt, maka akan dicek passwordnya apakah sudah benar atau belum.
```
if grep -q -w "${username} ${password}" user.txt; then
```

Jika password benar, maka akan menampilkan output login success dan juga mencetak string pada log.txt.
```
echo -e "\n${date} LOGIN SUCCESS" 
echo -e "\n${date} LOGIN: INFO User ${username} logged in" >> log.txt 
```

Jika password salah, maka akan menampilkan output wrong password dan juga mencetak string pada log.txt
```
echo -e "\n${date} WRONG PASSWORD" 
echo -e "\n${date} LOGIN: ERROR Failed login attempt on user ${username}" >> log.txt 
```

Variabel date merupakan variabel yang telah dibuat sebelumnya dan berisi data waktu saat dilakukan login tersebut.
```
date=$(date '+%Y/%m/%d %H:%M:%S')
```

Apabila user belum diregistrasi, maka akan diarahkan ke file louis.sh untuk registrasi terlebih dahulu
```
echo -e "\nERROR: User does not exist"
echo "You have to regist first"
bash louis.sh
```

Pada file louis.sh, akan mengecek apakah file user.txt dan log.txt sudah ada atau belum, jika sudah ada akan diskip dan tidak dibuatkan file baru lagi.
```
if [ ! -f "user.txt" ]; then
	touch user.txt
fi

if [ ! -f "log.txt" ]; then 
	touch log.txt
fi
```
Program akan mengecek kembali apakah username sudah pernah diregistrasi atau belum dengan mengeceknya di user.txt
```
if grep -q -w "${username}" user.txt ; then
```
Jika username sudah pernah diregistrasi, maka akan mengeluarkan output message error dan message error juga dicetak pada log.txt

```
echo "${date} REGISTER: ERROR User already exists"
echo -e  "\n${date} REGISTER: ERROR User already exists" >> log.txt
```

Pada registrasi akun, memiliki beberapa peraturan password yang harus terpenuhi yaitu 
1. password tidak boleh sama dengan username
```
if [[ ${username} == ${password} ]]; then
	echo "ERROR: Password must be different with username"
```
2. Password harus berisi minimal 1 uppercase
```
elif ! [[ ${password} =~ [A-Z] ]]; then
	echo "ERROR: Password must contain at least 1 uppercase"
```
3. Password harus berisi minimal 1 lowercase
```
elif ! [[ ${password} =~ [a-z] ]]; then 
	echo "ERROR: Password must contain at least 1 lowercase"
```
4. Panjang password tidak boleh kurang dari 8
```
elif [[ ${#password} -lt 8 ]]; then
	echo "ERROR: Password must than 8 characters"
```
4. Password harus mengandung minimal 1 angka (alphanumeric)
```
elif ! [[ ${password} =~ [0-9] ]]; then 
	echo "ERROR: Password must alphanumeric"
```
5. Password tidak boleh mengandung kata chicken dan ernie
```
elif [[ ${password}  =~ "chicken" ]]; then
	echo "ERROR: Password can not contain chicken"
elif [[ ${password}  =~ "ernie" ]]; then
	echo "ERROR: Password can not contain ernie"
```
Jika semua aturan password telah terpenuhi dan user belum pernah diregistrasi sebelumnya, maka akan menampikan message registrasi berhasil dan akan diprint pada file log.txt dan data user akan diprint pada user.txt.
```
echo -e "\n${date} REGISTRATION SUCCESS"
echo -e "\n${date} REGISTER: INFO User ${username} registered successfully" >> log.txt
echo "$username $password" >> user.txt
```


## ---soal4:
		---log_encrypt.sh
			---log_decrypt.sh

### Penjelasan
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup.
- Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
- Setelah huruf z akan kembali ke huruf a
- Buat juga script untuk dekripsinya.
- Backup file syslog setiap 2 jam untuk dikumpulkan.

### Penyelesaian
log_encrypt.sh

-	```
	rot=$(date +%H)
	```
	- Memasukan input jam saat ini kedalam variable `rot` yang akan dijadikan rotation dari encryption alfabet.

-	```
	cp /var/log/syslog $PWD
	```
	- Backup file syslog ke repository saat ini menggunakan fungsi `cp`.
	
-	```
	lower=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
	upper=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ
	```
	- Membuat variable `lower` dan `upper` yang berisi alfabet sebelum diencrypt.
	- `lower` berisi alfabet lowercase dan `upper` berisi alfabet uppercase.
	- Karena setelah z kembali ke a maka alfabet dituliskan dua kali.
	
-	```
	lower_encrypt=$(echo $lower | tr "${lower:0:26}" "${lower:${rot}:26}")
	upper_encrypt=$(echo $upper | tr "${upper:0:26}" "${upper:${rot}:26}")
	```
	- Melakukan translation menggunakan fungsi `tr` dengan input dari variable `upper` dan `lower`.
	- Translation dilakukan sebanyak jumlah huruf alfabet yaitu 26 huruf.
	- Rotasi cipher menggunakan `rot` sesuai jam saat ini, tiap alfabet akan bergeser ke kanan sebanyak `rot`.
	- Hasil cipher dimasukan kedalam variable `lower_encrypt` dan `upper_encrypt`.

-	```
	tr "${lower}${upper}" "${lower_encrypt}${upper_encrypt}" < $PWD/syslog > $PWD/syslogenc.txt
	mv syslogenc.txt "$(date +%H:%M\ %d:%m:%Y).txt"
	rm syslog
	```
	- Melakukan encrypt file syslog dengan input file yang telah dibackup.
	- Menggunakan fungsi `tr` untuk mentranslate alfabet dalam syslog dari alfabet biasa (`lower` & `upper`) menjadi alfabet yang telah dichiper (`lower_encrypt` & 	`upper_encrypt`).
	- Output hasil translate dimasukkan ke `syslogenc.txt`.
	- Rename file `syslogenc.txt` sesuai permintaan soal yaitu jam dan tanggal saat ini menggunakan fungsi `mv`.
	- Hapus file backup syslog sebelum diencrypt menggunakan `rm`.

-	```
	#0 */2 * * * $PWD/log_encrypt.sh
	```
	> Crob Jobs Settings
	- Menggunakan crob jobs untuk menjalankan script backup dua jam sekali.
	
log_decrypt.sh

-	```
	encrypt_file="$1"
	```
	- Menggunakan parameter `$1` sebagai input file yang akan didecrypt.
	- Simpan input kedalam variable `encrypt_file`

-	```
	rot=$(stat -c %n "$encrypt_file" | cut -d':' -f1)
	```
	- Mengambil jam sesuai file encrypt sebagai input rotation cipher untuk mengembalikan encryption ke semula.
	- Mengambil nama file encrypt_file menggunakan fungsi `stat` dengan parameter `-c %n` untuk set format mendisplay nama file dari `encrypt_file`.
	- Hanya mengambil jam dari nama file dan membuang sisanya menggunakan `cut` dengan set delimeter dan mengambil kata pertama menggunakan `-d':' -f1`.
	- Memasukan hasil kedalam variable `rot`.

-	```
	lower=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
	upper=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ
	```
	- Membuat variable `lower` dan `upper` yang berisi alfabet sebelum diencrypt.
	- `lower` berisi alfabet lowercase dan `upper` berisi alfabet uppercase.
	- Karena setelah z kembali ke a maka alfabet dituliskan dua kali.

-	```
	lower_encrypt=$(echo $lower | tr "${lower:0:26}" "${lower:${rot}:26}")
	upper_encrypt=$(echo $upper | tr "${upper:0:26}" "${upper:${rot}:26}")
	```
	- Melakukan translation menggunakan fungsi `tr` dengan input dari variable `upper` dan `lower`.
	- Translation dilakukan sebanyak jumlah huruf alfabet yaitu 26 huruf.
	- Rotasi cipher menggunakan `rot` sesuai jam saat file diencrypt, tiap alfabet akan bergeser ke kanan sebanyak `rot`.
	- Hasil cipher dimasukan kedalam variable `lower_encrypt` dan `upper_encrypt`.
