#!/bin/bash

#mengambil parameter file yang ingin didecrypt sesuai
encrypt_file="$1"

#mengambil jam sesuai nama file encrypt
rot=$(stat -c %n "$encrypt_file" | cut -d':' -f1)

#mengganti alfabet sesuai soal
lower=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
upper=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ

lower_encrypt=$(echo $lower | tr "${lower:0:26}" "${lower:${rot}:26}")
upper_encrypt=$(echo $upper | tr "${upper:0:26}" "${upper:${rot}:26}")

#decrypt file
tr "${lower_encrypt}${upper_encrypt}" "${lower}${upper}" < "$encrypt_file" > "decrypt_$encrypt_file"
